#include <stdlib.h>

#include <iostream>

#include <ctime>

#include <fstream>

using namespace std;

#define FILTER_SIZE 3
# define FILTERS 2
void applyFilters(int * image, int height, int width, int * filter, int filters) {
        int * copy = new int[height * width];
        int i, filter_index, h, w, h_width, h_wi_w, offx, offy, x_h_wid_w, offx_f, filterIndex;
        const int filtersize = FILTER_SIZE * FILTER_SIZE;
        filter_index = 0;
        for (h = 0; h < height; ++h) {
                h_width = h * width;
                for (w = 0; w < (width); ++w) {
                        h_wi_w = h_width + w;
                        copy[h_wi_w] = 0;
                        for (offx = -1; offx <= 1; ++offx) {
                                if (offx + h < 0 || offx + h >= height)
                                        continue;
                                x_h_wid_w = (offx + h) * width + w;
                                offx_f = (1 + offx) * FILTER_SIZE + 1;
                                filterIndex = filter_index + offx_f;
                                for (offy = -1; offy <= 1; ++offy) {
                                        if (offy + w < 0 || offy + w >= width)
                                                continue;
                                    //storing it onto the copy array
                                        copy[h_wi_w] += image[x_h_wid_w + offy] *
                                                filter[filterIndex + offy];
                        }
                        }
                        if((h-1)>=0 && (w-1)>=0) //checking whether the index is valid so that 2nd filter can be applied
                         {
                         int above_row=h-1;
                         int w_second_filter=w-1;
                            int t=(h-1)*width+w-1;
                            image[t]=0;
                             for (int offx_second = -1; offx_second <= 1; ++offx_second) {
                                if (offx_second + above_row < 0 || offx_second + above_row >= height)
                                        continue;
                                                  int t1=filtersize + (1 + offx_second) * FILTER_SIZE + 1;
                                                  int t2=(offx_second + above_row) * width;
                                    for (int offy_second = -1; offy_second <= 1; ++offy_second) {
                                                if ( offy_second + w_second_filter < 0 || offy_second + w_second_filter >= width)
                                                        continue;
                                                image[t] += copy[t2 + offy_second + w_second_filter] *
                                                        filter[t1 + offy_second];
                                        }
                                } // computing the 2nd filter and storing on the image array
                        }

                }
                //computing 2nd filter for the last element in the row
                         int above_row=h-1;
                         int w_second_filter=w-1;
                            int t=(h-1)*width+w-1;
                            image[t]=0;
                             for (int offx_second = -1; offx_second <= 1; ++offx_second) {
                                if (offx_second + above_row < 0 || offx_second + above_row >= height)
                                        continue;
                                                  int t1=filtersize + (1 + offx_second) * FILTER_SIZE + 1;
                                                  int t2=(offx_second + above_row) * width;
                                    for (int offy_second = -1; offy_second <= 1; ++offy_second) {
                                                if ( offy_second + w_second_filter < 0 || offy_second + w_second_filter >= width)
                                                        continue;
                                                image[t] += copy[t2 + offy_second + w_second_filter] *
                                                        filter[t1 + offy_second];
                                        }
                                }



        }//computing the 2nd filter for the last row
        for(int w_second_filter=0;w_second_filter < width;w_second_filter++)
        {
             int above_row=h-1;
                         int t=(h-1)*width+w_second_filter;
                            image[t]=0;
                             for (int offx_second = -1; offx_second <= 1; ++offx_second) {
                                if (offx_second + above_row < 0 || offx_second + above_row >= height)
                                        continue;
                                                  int t1=filtersize + (1 + offx_second) * FILTER_SIZE + 1;
                                                  int t2=(offx_second + above_row) * width;
                                    for (int offy_second = -1; offy_second <= 1; ++offy_second) {
                                                if ( offy_second + w_second_filter < 0 || offy_second + w_second_filter >= width)
                                                        continue;
                                                image[t] += copy[t2 + offy_second + w_second_filter] *
                                                        filter[t1 + offy_second];
                                        }
                                }
        }

}
int main() {
        int ht, wd;
        cin >> ht >> wd;
        int * img = new int[ht * wd];
 for (int i = 0; i < ht; ++i)
                for (int j = 0; j < wd; ++j)
                        cin >> img[i * wd + j];

        int filters = FILTERS;
        int * filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
        for (int i = 0; i < filters; ++i)
                for (int j = 0; j < FILTER_SIZE; ++j)
                        for (int k = 0; k < FILTER_SIZE; ++k)
                                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];

        clock_t begin = clock();
        applyFilters(img, ht, wd, filter, filters);
        clock_t end = clock();
        cout << "Execution time: " << double(end - begin) / (double) CLOCKS_PER_SEC << " seconds\n";
        ofstream fout("output.txt");
        for (int i = 0; i < ht; ++i) {
                for (int j = 0; j < wd; ++j)
                        fout << img[i * wd + j] << " ";
                fout << "\n";
        }
        }



